<?php
require_once "animal.php";
require_once "frog.php";
require_once "ape.php";


$sheep = new Animal("shaun", "4");

echo "Nama : " . $sheep->name . "<br> Legs : "; // "shaun"
echo $sheep->leg . "<br> cold blooded : "; // 4
echo $sheep->cold_blooded() . "<br><br>"; // "no"

$frog = new Frog("buduk", "4");
echo "Nama : " . $frog->name . "<br> Legs : ";
echo $frog->leg . "<br> cold blooded : "; // 4
echo $frog->cold_blooded() . "<br> Jump : "; // "no"
echo $frog->jump() . "<br><br>";

$ape = new Ape("kera sakti", "2");
echo "Nama : " . $ape->name . "<br> Legs : ";
echo $ape->leg . "<br> cold blooded : "; // 4
echo $ape->cold_blooded() . "<br> Yell : "; // "no"
echo $ape->yell() . "<br><br>";
// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())